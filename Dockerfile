include(bullseye)

RUN apt install --yes wget gnupg apt-transport-https
RUN wget -O - https://lemonldap-ng.org/_media/rpm-gpg-key-ow2 | apt-key add - && \
    echo "deb https://lemonldap-ng.org/deb stable main" > /etc/apt/sources.list.d/lemonldap-ng.list && \
    apt update && \
    apt install --yes lemonldap-ng