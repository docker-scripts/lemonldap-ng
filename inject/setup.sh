#!/bin/bash -x

source /host/settings.sh

# fix the domain on the config files
sed -i \
    -e "s/example\.com/$SSO_DOMAIN/g" \
    /etc/lemonldap-ng/* \
    /var/lib/lemonldap-ng/conf/lmConf-1.json

# fix nginx config
cd /etc/nginx/sites-enabled/
ln -s /etc/lemonldap-ng/api-nginx.conf
ln -s /etc/lemonldap-ng/handler-nginx.conf
ln -s /etc/lemonldap-ng/manager-nginx.conf
ln -s /etc/lemonldap-ng/portal-nginx.conf
ln -s /etc/lemonldap-ng/test-nginx.conf
systemctl restart nginx

# set an alias for lemonldap-ng-cli
ln -s /usr/share/lemonldap-ng/bin/lemonldap-ng-cli /usr/local/bin/llng

# setup LDAP authentication
llng -yes 1 \
     set \
     authentication LDAP \
     userDB LDAP \
     passwordDB LDAP \
     registerDB LDAP \
     ldapServer $LDAP_SERVER \
     ldapBase $LDAP_BASE \
     managerDn $LDAP_BIND_DN \
     managerPassword $LDAP_BIND_PW
llng -yes 1 \
     addKey \
     ldapExportedVars uid uid \
     ldapExportedVars cn cn \
     ldapExportedVars sn sn \
     ldapExportedVars mail mail \
     ldapExportedVars givenName givenName

# remove unused rules for accessing manager
llng -yes 1 \
     delKey \
     locationRules/manager.$SSO_DOMAIN '(?#Configuration)^/(.*?\.(fcgi|psgi)/)?(manager\.html|confs/|$)' \
     locationRules/manager.$SSO_DOMAIN '(?#Sessions)/(.*?\.(fcgi|psgi)/)?sessions' \
     locationRules/manager.$SSO_DOMAIN '(?#Notifications)/(.*?\.(fcgi|psgi)/)?notifications'
# change the default rule for accessing manager
llng -yes 1 \
     addKey \
     locationRules/manager.$SSO_DOMAIN default "\$uid =~ /^$ADMIN\$/"
