APP=lemonldap-ng

SSO_DOMAIN='example.org'
DOMAIN='sso.example.org'
DOMAINS='
    auth.example.org
    manager.example.org
    test1.example.org
    test1.example.org
'

LDAP_SERVER='ldap://ldap.example.org'
LDAP_BASE='ou=users,dc=example,dc=org'
LDAP_BIND_DN='cn=auth,ou=apps,dc=example,dc=org'
LDAP_BIND_PW='pass123'

ADMIN='admin@example.org'
