# LemonLDAP::NG

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull lemonldap-ng`

  - Initialize a directory: `ds init lemonldap-ng @sso.example.org`

  - Fix the settings: `cd /var/ds/sso.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

## Other commands

```
ds stop
ds start
ds shell
ds help
```
